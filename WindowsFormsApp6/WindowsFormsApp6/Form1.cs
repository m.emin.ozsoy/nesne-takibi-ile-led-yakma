﻿using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using AForge.Imaging.Filters;
using AForge.Imaging;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Drawing.Imaging;


namespace WindowsFormsApp6
{
    public partial class Form1 : Form
    {
        private FilterInfoCollection kamerasayisi; // video cihazını yani kameraları bulmak için ve kaç kamera bağlı olduğunu tutan dizi
        private VideoCaptureDevice kamera;
        public Form1()
        {
            InitializeComponent();
            serialPort1.PortName = "COM3";
            serialPort1.BaudRate = 9600;


        }

        int R; //Trackbarın değişkeneleri
        int G;
        int B;
        
        private void button1_Click(object sender, EventArgs e)
        {   //videoyu kare kare olarak seçilen kameradan alıyor 
            kamera = new VideoCaptureDevice(kamerasayisi[comboBox1.SelectedIndex].MonikerString); //combobox daki kamera video kaynağını alıyor.
            kamera.NewFrame += new NewFrameEventHandler(Finalvideo_NewFrame);              
            kamera.Start(); //videoyu başlatıyor

        }

        private void Form1_Load(object sender, EventArgs e)

        {    
            kamerasayisi = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo VideoCaptureDevice in kamerasayisi)
            {

                comboBox1.Items.Add(VideoCaptureDevice.Name);

            }

            comboBox1.SelectedIndex = 0;
            //combobox da bilgisayatrın kamerasını seçiyoruz.
        }
       private void Finalvideo_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            //kullanılacak cihaz METODU oluşturuluyor.
            Bitmap image = (Bitmap)eventArgs.Frame.Clone();
            Bitmap image1 = (Bitmap)eventArgs.Frame.Clone();
            pictureBox1.Image = image;




            if (radioButton1.Checked)
            {
                //3 boyutlu öklid bağıntısını kullanır. Değerlere göre bir merkez belirler ve yarıçapa göre küre oluşturarak pikselleri tutar. 

                EuclideanColorFiltering filter = new EuclideanColorFiltering();              
                filter.CenterColor = new RGB(Color.FromArgb(215, 0, 0));
                filter.Radius = 100;            
                filter.ApplyInPlace(image1);

                nesnebul(image1);


            }
            if (radioButton2.Checked)
            {

                EuclideanColorFiltering filter = new EuclideanColorFiltering();
                filter.CenterColor = new RGB(Color.FromArgb(0, 215, 0));
                filter.Radius = 100;
                filter.ApplyInPlace(image1);

                nesnebul(image1);

            }

            if (radioButton3.Checked)
            {
             
                EuclideanColorFiltering filter = new EuclideanColorFiltering();               
                filter.CenterColor = new RGB(Color.FromArgb(30, 144, 255));
                filter.Radius = 100;
                filter.ApplyInPlace(image1);

                nesnebul(image1);


            }

            if (radioButton4.Checked)
            {

                EuclideanColorFiltering filter = new EuclideanColorFiltering();
                filter.CenterColor = new RGB(Color.FromArgb(R, G, B));
                filter.Radius = 100;
                filter.ApplyInPlace(image1);

                nesnebul(image1);


            }

        }
        public void nesnebul(Bitmap image)

        {
           
            BlobCounter blobCounter = new BlobCounter();
            blobCounter.MinWidth = 10;
            blobCounter.MinHeight = 10;
            blobCounter.FilterBlobs = true;
            blobCounter.ObjectsOrder = ObjectsOrder.Size;
            //Görüntü üzerindeki nesneleri saymak, bilgilerini elde etmek için kullanılır. 
            blobCounter.ProcessImage(image);
            Rectangle[] rects = blobCounter.GetObjectsRectangles();
            Blob[] blobs = blobCounter.GetObjectsInformation(); //en büyük bloğu al
            pictureBox2.Image = image;
            

            foreach (Rectangle recs in rects)
                {

                   if (rects.Length > 0)
                   {
                    Rectangle objectRect = rects[0];
                    Graphics g = pictureBox1.CreateGraphics();
                    using (Pen pen = new Pen(Color.FromArgb(252, 3, 26), 2))
                    {
                        g.DrawRectangle(pen, objectRect);
                    }
                    //Cizdirilen Dikdörtgenin Koordinatlari aliniyor.
                    int objectX = objectRect.X + (objectRect.Width / 2);
                    int objectY = objectRect.Y + (objectRect.Height / 2);
                    g.DrawString(objectX.ToString() + "X" + objectY.ToString(), new Font("Arial", 12), Brushes.Red, new System.Drawing.Point(250, 1));
                 

                      if (objectX <= 213 && objectY <= 160)
                      {

                        serialPort1.Open();
                        if (serialPort1.IsOpen)
                        {
                            serialPort1.WriteLine("1");
                        }
                        serialPort1.Close();

                      }

                      else if (objectX > 213 && objectX <= 427 && objectY <= 160)

                      {

                        serialPort1.Open();
                        if (serialPort1.IsOpen)
                        {
                            serialPort1.WriteLine("2");
                        }
                        serialPort1.Close();

                      }
                      else if (objectX > 427 && objectY <= 160)
                      {

                        serialPort1.Open();
                        if (serialPort1.IsOpen)
                        {
                            serialPort1.WriteLine("3");
                        }
                        serialPort1.Close();

                      }
                      else if (objectX <= 213 && objectY > 160 && objectY <= 320)
                      {

                        serialPort1.Open();
                        if (serialPort1.IsOpen)
                        {
                            serialPort1.WriteLine("4");
                        }
                        serialPort1.Close();

                      }
                      else if (objectX > 213 && objectX <= 427 && objectY > 160 && objectY <= 320)
                      {

                        serialPort1.Open();
                        if (serialPort1.IsOpen)
                        {
                            serialPort1.WriteLine("5");
                        }
                        serialPort1.Close();

                      }
                      else if (objectX > 427 && objectY > 160 && objectY <= 320)
                      {

                        serialPort1.Open();
                        if (serialPort1.IsOpen)
                        {
                            serialPort1.WriteLine("6");
                        }
                        serialPort1.Close();

                      }
                      else if (objectX < 213 && objectY > 320)
                      {

                        serialPort1.Open();
                        if (serialPort1.IsOpen)
                        {
                            serialPort1.WriteLine("7");
                        }
                        serialPort1.Close();

                      }
                      else if (objectX > 213 && objectX < 427 && objectY > 320)
                      {

                        serialPort1.Open();
                        if (serialPort1.IsOpen)
                        {
                            serialPort1.WriteLine("8");
                        }
                        serialPort1.Close();

                      }

                      else if (objectX > 427 && objectY > 320)
                      {

                        serialPort1.Open();
                        if (serialPort1.IsOpen)
                        {
                            serialPort1.WriteLine("9");
                        }
                        serialPort1.Close();

                      }

                   }
                 
            }

        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            R = trackBar1.Value;
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            G = trackBar2.Value;
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            B = trackBar3.Value;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            serialPort1.Open();
            if (serialPort1.IsOpen)
            {
                serialPort1.WriteLine("0");
            }
            serialPort1.Close();

            if (kamera.IsRunning)
            {
                kamera.Stop();

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
         
             if (kamera.IsRunning)
             {
                 kamera.Stop();

             }
                
            serialPort1.Open();
            if (serialPort1.IsOpen)
            {
                serialPort1.WriteLine("0");
            }
            serialPort1.Close();

            Application.Exit();
        }
    }
}
       

        
   



        
   


    

       
   

